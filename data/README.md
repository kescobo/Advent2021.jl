# Getting Data

## Set session cookie

Use `.envrc` or other method to set environment variable `AOC_SESSION`,
eg

```
# .envrc
layout julia
export AOC_SESSION="<cookie here>"
```

The cookie can be found using developer tools in your browser
(in vivaldi -> "Application" -> "Storage" -> "Cookies").
It's a long (~96 digit) hexadecimal key.

The file should automatically be downloaded using `solve`,
but if you want to download manually,

```julia
using Advent2021

Advent2021._download_data(Advent2021.Day(1))
```