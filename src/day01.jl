function solve1(io, ::Day{1})
    current_depth = nothing
    stream(io) do line
        depth = parse(Int, line)
        increased = !isnothing(current_depth) && current_depth < depth
        current_depth = depth
        return increased
    end |> count
end

function solve2(io, ::Day{1})
    depths = collect(stream(x-> parse(Int, x), io))
    count(i-> depths[i] > depths[i-3], 4:length(depths))
end

const day1_test = """
199
200
208
210
200
207
240
269
260
263
"""

@testset "day01" begin
    @test solve1(IOBuffer(day1_test), Day(1)) == 7
    @test solve2(IOBuffer(day1_test), Day(1)) == 5

    @test solve(Day(1)) == (1475, 1516)
end