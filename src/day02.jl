function solve1(io, ::Day{2})
    depth = 0
    pos = 0
    foreach(identity, stream(io) do line
        (direction, inc) = split(line)
        inc = parse(Int, inc)
        if direction == "forward"
            pos += inc
        elseif direction == "down"
            depth += inc
        elseif direction == "up"
            depth -= inc
        else
            error("parsing error")
        end
    end)
    return depth * pos
end

function solve2(io, ::Day{2})
    depth = 0
    pos = 0
    aim = 0
    foreach(identity, stream(io) do line
        (direction, inc) = split(line)
        inc = parse(Int, inc)
        if direction == "forward"
            pos += inc
            depth += aim * inc
        elseif direction == "down"
            aim += inc
        elseif direction == "up"
            aim -= inc
        else
            error("parsing error")
        end
    end)
    return depth * pos
end

const day2_test = """
forward 5
down 5
forward 8
up 3
down 8
forward 2
"""

@testset "day02" begin
    @test solve1(IOBuffer(day2_test), Day(2)) == 150
    @test solve2(IOBuffer(day2_test), Day(2)) == 900

    @test solve(Day(2)) == (1746616, 1741971043)
end