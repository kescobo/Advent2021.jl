get_bitmat(io) = reduce(hcat,
    stream(line-> [parse(Bool, c) for c in line], io)
) |> permutedims

function solve1(io, ::Day{3})
    bitmat = get_bitmat(io)
    common = map(eachcol(bitmat)) do col
        count(col) / length(col) > 0.5
    end

    γ = parse(Int, join(Int.(common)), base=2)
    ϵ = parse(Int, join(Int.(.!common)), base=2)

    γ * ϵ
end

function solve2(io, ::Day{3})
    bitmat = get_bitmat(io)
    O₂_keep = trues(size(bitmat, 1))
    O₂ = nothing
    for col in eachcol(bitmat)
        O₂_ratio = count(col[O₂_keep]) / length(col[O₂_keep])
        O₂_common = O₂_ratio >= 0.5
        O₂_keep = O₂_keep .& map(==(O₂_common), col)
        if length(col[O₂_keep]) == 1 
            O₂ = parse(Int, join(Int.(bitmat[O₂_keep, :])), base=2)
            break
        end
    end
    
    CO₂_keep = trues(size(bitmat, 1))
    CO₂ = nothing
    for col in eachcol(bitmat)
        CO₂_ratio = count(col[CO₂_keep]) / length(col[CO₂_keep])
        CO₂_common = CO₂_ratio >= 0.5
        CO₂_keep = CO₂_keep .& map(!=(CO₂_common), col)
        if length(col[CO₂_keep]) == 1 
            CO₂ = parse(Int, join(Int.(bitmat[CO₂_keep, :])), base=2)
            break
        end
    end


    return O₂ * CO₂
end

const day3_test = """
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
"""

@testset "day03" begin
    @test solve1(IOBuffer(day3_test), Day(3)) == 198
    @test solve2(IOBuffer(day3_test), Day(3)) == 230

    @test solve(Day(3)) == (3813416, 2990784)
end