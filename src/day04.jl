struct BingoBoard
    mat::Matrix{Int}
    hits::Matrix{Bool}
end

function BingoBoard(nums::Vector{<:AbstractString})
    BingoBoard(
        reshape(parse.(Int, nums), 5,5),
        falses(5,5)
    )
end


function bingo(bb::BingoBoard)
    any(all, eachcol(bb.hits)) ||
    any(all, eachrow(bb.hits))
end


function score!(bb::BingoBoard, num)
    hits = findall(==(num), bb.mat)
    if !isempty(hits)
        bb.hits[hits] .= true
        return bingo(bb)
    else
        return false
    end
end


function parseinput(io, ::Day{4})
    winners = Int[]
    boards = BingoBoard[]
    currentboard = ""
    for (i, line) in enumerate(stream(identity, io))
        if i == 1
            winners = parse.(Int, split(line, ','))
        elseif i == 2
            continue
        else
            if !isempty(line)
                currentboard = string(currentboard, " ", line)
            else
                push!(boards, BingoBoard(filter(!isempty, split(currentboard, r" +"))))
                currentboard = ""
            end
        end
    end
    push!(boards, BingoBoard(filter(!isempty, split(currentboard, r" +"))))

    return winners, boards
end

function solve1(io, ::Day{4})
    (winners, boards) = parseinput(io, Day(4))

    win = nothing

    for winner in winners
        iswin = findfirst(bb-> score!(bb, winner), boards)
        if !isnothing(iswin)
            win = (boards[iswin], winner)
            break
        end
    end
    return sum(win[1].mat[.!win[1].hits]) * win[2]
end

function solve2(io, ::Day{4})
    (winners, boards) = parseinput(io, Day(4))

    win = nothing

    for winner in winners
        iswin = findall(bb-> score!(bb, winner), boards)
        if !isempty(iswin)
            winboards = splice!(boards, iswin)
            if isempty(boards)
                length(winboards) > 1 && error()
                win = (first(winboards), winner)
                break
            end
        end
    end
    
    return sum(win[1].mat[.!win[1].hits]) * win[2]
end

const day4_test = """
7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
"""

@testset "day04" begin
    @test solve1(IOBuffer(day4_test), Day(4)) == 4512
    @test solve2(IOBuffer(day4_test), Day(4)) == 1924

    # @test solve(Day(4)) == (YY, YY)
end