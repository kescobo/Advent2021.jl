neighbors(mat, i) = (mat[i, 1], mat[i, 3], mat[i-1, 2], mat[i+1, 2])

function checksafe!(mat, filler, idx)
    mat[idx, 1:2] .= mat[idx, 2:3]
    mat[idx, 3] .= filler
    safespots = findall(idx) do i
        all(x-> x > mat[i, 2], neighbors(mat, i))
    end

    return isempty(safespots) ? 0 : sum(1 .+ mat[idx[safespots], 2])
end

function solve1(io, ::Day{9})
    streamer = stream(line -> parse.(Int, (line[i] for i in eachindex(line))), io)
    
    heights = first(streamer)
    collength = length(heights)

    rolling = fill(9, collength +2, 3)
    idx = 2:collength+1

    safe = 0
    for line in streamer
        safe += checksafe!(rolling, heights, idx)
        heights = line
    end
    safe += checksafe!(rolling, heights, idx)
    safe += checksafe!(rolling, fill(9, collength), idx)

    return safe
end

function solve2(io, ::Day{9})
    
end

const day09_test = """
2199943210
3987894921
9856789892
8767896789
9899965678
"""

@testset "day09" begin
    @test solve1(IOBuffer(day09_test), Day(9)) == 15
    # @test solve2(IOBuffer(day09_test), Day(9)) == YY

    # @test solve(Day(09)) == (486, YY)
end