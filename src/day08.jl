function parse_line(line, ::Day{8})
    signal = split(line, r"[ |]", keepempty=false)
    digs = splice!(signal, 11:14)
    return (signal, digs)
end

function solve1(io, ::Day{8})
    sum(stream(line-> parse_line(line, Day(8)), io)) do (signal, digs)
        count(dig-> length(dig) in (2, 3, 4, 7), digs)
    end
end

segments(seg) = Set((c for c in seg)) 

function solve2(io, ::Day{8})

    for (signal, digs) in stream(line-> parse_line(line, Day(8)), io)
        @info signal
        signal = join(signal)
        wts = Dict(c => count(==(c), signal) for c in collect("abcdefg"))
        return wts
    end
end

const day08_test = """
be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
"""

@testset "day08" begin
    @test solve1(IOBuffer(day08_test), Day(8)) == 26
    # @test solve2(IOBuffer(day08_test), Day(8)) == 61229

    # @test solve(Day(08)) == (375, YY)
end