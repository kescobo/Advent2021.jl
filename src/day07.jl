function solve1(io, ::Day{7})
    pos = stream(x-> parse.(Int, split(x, ',')), io) |> first
    meet = round(Int, median(pos))
    sum(x-> abs(x-meet), pos)
end

fuelconsumption(pos, meet) = sum(x-> sum(1:abs(x-meet)), pos)

function solve2(io, ::Day{7})
    pos = stream(x-> parse.(Int, split(x, ',')), io) |> first
    meet = Int(median(pos))
    fuel = fuelconsumption(pos, meet)

    incr = fuelconsumption(pos, meet-1) < fuel ? -1 : 1
    while true
        meet += incr
        curfuel = fuelconsumption(pos, meet)
        curfuel > fuel && return fuel
        fuel = curfuel
    end
end

const day07_test = """
16,1,2,0,4,2,7,1,2,14
"""

@testset "day07" begin
    @test solve1(IOBuffer(day07_test), Day(7)) == 37
    @test solve2(IOBuffer(day07_test), Day(07)) == 168

    # @test solve(Day(07)) == (348996, )
end