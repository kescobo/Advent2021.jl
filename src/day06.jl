function parseinput(io, pop, ::Day{6})
    foreach(identity, stream(io) do line
        for n in split(line, ',')
            c = parse(Int, string(n))
            pop[c] += 1
        end
    end)
    return pop
end

function solve1(io, ::Day{06}; days=8)
    population = Dict(x=>0 for x in 0:8)
    generation = Dict(x=>0 for x in 0:8)
    
    parseinput(io, population, Day(6))
    # @info [k => population[k] for k in 0:8]
    for _ in 1:days
        if population[0] != 0
            generation[8] = population[0]
            generation[6] = population[0]
        end

        for k in 1:8
            population[k] == 0 && continue
            generation[k-1] += population[k]
        end
        foreach(k-> population[k] = generation[k], 0:8)
        foreach(k-> generation[k] = 0, 0:8)
        # @info [k => population[k] for k in 0:8]
    end
    return sum(values(population))
end

solve2(io, ::Day{06}) = solve1(io, Day(6), days=256)

const day06_test = """
3,4,3,1,2
"""

@testset "day06" begin
    @test solve1(IOBuffer(day06_test), Day(06), days=80) == 5934
    @test solve2(IOBuffer(day06_test), Day(06)) == 26984457539

    # @test solve(Day(06)) == (351188, YY)
end