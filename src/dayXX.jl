# This code will not run, it's intended as a template
# Find and replace 'XX' with the day number, and YY with the day's inputs

function solve1(io, ::Day{XX})
    
end

function solve2(io, ::Day{XX})
 
end

const dayXX_test = """
YY
"""

@testset "dayXX" begin
    @test solve1(IOBuffer(dayXX_test), Day(XX)) == YY
    # @test solve2(IOBuffer(dayXX_test), Day(XX)) == YY

    # @test solve(Day(XX)) == (YY, YY)
end