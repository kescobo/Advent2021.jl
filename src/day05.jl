function parseinput(io, ::Day{5})
    stream(io) do line
        m = match(r"(\d+),(\d+) -> (\d+),(\d+)", line)
        x1, y1, x2, y2 = parse.(Int, string.(m.captures))
        return (x1, y1), (x2, y2)
    end
end

getrange(x1,x2) = x1 <= x2 ? (x1:x2) : (x1:-1:x2)


function solve1(io, ::Day{5})
    pts = Dict{Tuple{Int,Int},Int}()
    
    for ((x1, y1), (x2, y2)) in parseinput(io, Day(5))
        if x1 == x2
            foreach(y-> (pts[(x1, y)] = get(pts, (x1,y), 0) + 1), getrange(y1, y2))
        elseif y1 == y2
            foreach(x-> (pts[(x, y1)] = get(pts, (x,y1), 0) + 1), getrange(x1, x2)) 
        end
    end
    count(>=(2), values(pts))
end

function solve2(io, ::Day{5})
    pts = Dict{Tuple{Int,Int},Int}()

    for ((x1, y1), (x2, y2)) in parseinput(io, Day(5))
        if x1 == x2
            foreach(y-> (pts[(x1, y)] = get(pts, (x1,y), 0) + 1), getrange(y1, y2))
        elseif y1 == y2
            foreach(x-> (pts[(x, y1)] = get(pts, (x,y1), 0) + 1), getrange(x1, x2)) 
        else        
            for (x,y) in zip(getrange(x1,x2), getrange(y1,y2))
                pts[(x, y)] = get(pts, (x,y), 0) + 1
            end
        end

    end

    count(>=(2), values(pts))
end


const day05_test = """
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
"""

@testset "day05" begin
    @test solve1(IOBuffer(day05_test), Day(5)) == 5
    @test solve2(IOBuffer(day05_test), Day(5)) == 12

    @test solve(Day(5)) == (7438, 21406)
end